from flask import jsonify, request
from datetime import datetime
from app.models.issues import Issue
from app.models.machines import Machines
from app.models.issue_status_history import IssueStatusHistory
from app import db
from app.schemas.issues_schema import IssueSchema, MachineIssueCountSchema
from app.schemas.issue_status_history_schema import IssueStatusHistorySchema
from collections import Counter
import re

# --------- 2. Implement an API endpoint that allows users to create a new report.
# --------- Each report should have a title, content, and be associated with a specific machine.
def ctrlr_issue_report_create(request):
    try:
        data = request.json
        id = data.get('id')
        machine_id = data.get('machine_id')
        issue_title = data.get('issue_title')
        description = data.get('description')
        status = data.get('status')

        new_report = Issue(
            id=id,
            machine_id=machine_id,
            issue_title=issue_title,
            description=description,
            timestamp=datetime.utcnow(),
            status=status,
            created_at=datetime.utcnow(),
            updated_at=datetime.utcnow()
        )

        db.session.add(new_report)
        db.session.commit()

        issue_schema = IssueSchema()
        new_report = issue_schema.dump(new_report)

        return jsonify(new_report), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500


# --------- 3. Retrive all issue reports:
# --------- 4. Filter issue reports:
def ctrlr_issue_report_list(request):
    try:
        machine_id = request.args.get('machine_id')
        title = request.args.get('title')
        description = request.args.get('description')
        start_timestamp = request.args.get('start_timestamp')
        end_timestamp = request.args.get('end_timestamp')
        status = request.args.get('status')

        query = db.session.query(Issue)

        if machine_id:
            query = query.filter(Issue.machine_id == machine_id)
        if title:
            query = query.filter(Issue.issue_title.ilike(f'%{title}%'))
        if description:
            query = query.filter(Issue.description.ilike(f'%{description}%'))
        if start_timestamp:
            start_date = datetime.strptime(start_timestamp, '%Y-%m-%d')
            query = query.filter(Issue.timestamp >= start_date)
        if end_timestamp:
            end_date = datetime.strptime(end_timestamp, '%Y-%m-%d')
            query = query.filter(Issue.timestamp <= end_date)
        if status:
            query = query.filter(Issue.status == status)

        issues = query.all()
        issues_schema = IssueSchema(many=True)
        issues_data = issues_schema.dump(issues)

        return jsonify(issues_data), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


# --------- 5. API for Counting Issue Reports Per Machine:
def ctrlr_count_issue_reports_per_machine():
    try:
        machines = Machines.query.all()

        machine_issue_counts = []
        for machine in machines:
            issue_count = Issue.query.filter_by(machine_id=machine.id).count()
            machine_data = {
                "machine_id": machine.id,
                "issue_count": issue_count
            }
            machine_issue_counts.append(machine_data)

        machine_issue_counts.sort(key=lambda x: x["issue_count"], reverse=True)

        schema = MachineIssueCountSchema(many=True)
        result = schema.dump(machine_issue_counts)

        return jsonify(result), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


# --------- 6. Count Common Words Across All Issue Titles and Descriptions with Dynamically Specified Top K:
def ctrlr_top_k_common_words(top_k):
    try:
        issues = Issue.query.all()

        combined_text = ' '.join(
            [issue.issue_title + ' ' + issue.description for issue in issues])
        words = re.findall(r'\w+', combined_text.lower())
        word_counts = Counter(words)
        top_k_words = word_counts.most_common(top_k)
        return jsonify([{"word": word, "frequency": count} for word, count in top_k_words]), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


# --------- 7.1 Resolving and History endpoint
def ctrlr_resolve_issue(issue_id, request):
    try:
        data = request.json
        id = data.get('id')
        new_status = data.get('new_status')
        comment = data.get('comment')

        issue = Issue.query.get(issue_id)
        if not issue:
            raise Exception("Issue not found")

        # ------ begin a transaction
        db.session.begin(subtransactions=True)

        # ------ load issue with exclusive lock
        loaded_issue = Issue.query.with_for_update().get(issue_id)
        if not loaded_issue:
            db.session.rollback()
            raise Exception("Issue not found")

        if loaded_issue.version != issue.version:
            db.session.rollback()
            raise Exception("Issue has been updated by another user")

        old_status = issue.status
        loaded_issue.status = new_status
        loaded_issue.version += 1

        status_history = IssueStatusHistory(
            id=id,
            issue_id=issue_id,
            old_status=old_status,
            new_status=new_status,
            comment=comment,
            timestamp=datetime.utcnow(),
            created_at=datetime.utcnow(),
            updated_at=datetime.utcnow()

        )
        db.session.add(status_history)
        db.session.commit()

        issue_with_history = db.session.query(Issue, IssueStatusHistory).join(
            IssueStatusHistory,
            Issue.id == IssueStatusHistory.issue_id
        ).filter(
            Issue.id == issue_id
        ).all()

        issue_with_history_schema = IssueStatusHistorySchema(many=True)
        issue_with_history_data = issue_with_history_schema.dump(
            issue_with_history)

        return jsonify(issue_with_history_data), 200
    except Exception as e:
        db.session.rollback()
        raise e

# In this version of the function, the version column of the Issue model is used to track changes.
# Before updating the issue's status, the loaded issue's version is checked against the original
# issue's version. If they don't match, it means there was a concurrent update,
# and the transaction is rolled back.

# This optimistic concurrency control mechanism helps prevent conflicts and ensures
# that updates to the issue status are synchronized and consistent.
