from flask import jsonify
from datetime import datetime
from app.models.machines import Machines
from app import db
from app.schemas.machines_schema import MachineSchema

# --------- 1. Design and Implement an API endpoint that allows registering a new machine with their info
def ctrlr_register_machine(request):
    try:
        data = request.json
        name = data.get('name')
        id = data.get('id')

        if not name:
            return jsonify({"error": "Machine name is required"}), 400

        new_machine = Machines(
            name=name,
            id=id,
            created_at=datetime.utcnow(),
            updated_at=datetime.utcnow())

        db.session.add(new_machine)
        db.session.commit()

        machine_schema = MachineSchema()
        machine_data = machine_schema.dump(new_machine)

        return jsonify(machine_data), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500


def ctrlr_machine_list():
    try:
        machines = Machines.query.all()
        machine_schema = MachineSchema(many=True)
        machines_data = machine_schema.dump(machines)

        return jsonify(machines_data), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
