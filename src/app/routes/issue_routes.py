from flask import Blueprint, request
from app.controllers.issue_controller import (
    ctrlr_issue_report_create,
    ctrlr_issue_report_list,
    ctrlr_count_issue_reports_per_machine,
    ctrlr_top_k_common_words,
    ctrlr_resolve_issue
)

issue_bp = Blueprint('issue', __name__)


@issue_bp.route('/create', methods=['POST'])
def issue_report_create_route():
    return ctrlr_issue_report_create(request)


@issue_bp.route('/list', methods=['GET'])
def issue_report_list_route():
    return ctrlr_issue_report_list(request)


@issue_bp.route('/issue-reports-per-machine', methods=['GET'])
def count_issue_reports_per_machine_route():
    return ctrlr_count_issue_reports_per_machine()


@issue_bp.route('/top-common-words', methods=['GET'])
def top_common_words_route():
    top_k = int(request.args.get('top_k', 5))
    return ctrlr_top_k_common_words(top_k)


@issue_bp.route('/resolve/<int:issue_id>', methods=['POST'])
def resolve_issue_route(issue_id):
    return ctrlr_resolve_issue(issue_id, request)
