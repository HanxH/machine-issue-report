from flask import Blueprint

# Initialize blueprints for machine and issue routes
machine_bp = Blueprint('machine', __name__)
issue_bp = Blueprint('issue', __name__)

# Import routes to register them
from . import machine_routes, issue_routes
