from app.controllers.machine_controller import (
    ctrlr_register_machine,
    ctrlr_machine_list,
)
from flask import Blueprint, request
machine_bp = Blueprint('machine', __name__)


@machine_bp.route('/register', methods=['POST'])
def register_machine_route():
    return ctrlr_register_machine(request)


@machine_bp.route('/list', methods=['GET'])
def machine_list_route():
    return ctrlr_machine_list()
