from marshmallow import Schema, fields

class IssueStatusHistorySchema(Schema):
    id = fields.Integer()
    issue_id = fields.Integer()
    old_status = fields.String()
    new_status = fields.String()
    comment = fields.String()
    timestamp = fields.DateTime()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    deleted = fields.Boolean()
