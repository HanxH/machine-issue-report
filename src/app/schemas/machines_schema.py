from marshmallow import Schema, fields

class MachineSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
