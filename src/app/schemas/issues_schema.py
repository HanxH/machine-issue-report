from marshmallow import Schema, fields

class IssueSchema(Schema):
    id = fields.Int()
    machine_id = fields.Int()
    issue_title = fields.Str()
    description = fields.Str()
    timestamp = fields.DateTime()
    status = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    deleted = fields.Bool()


class MachineIssueCountSchema(Schema):
    machine_id = fields.Integer()
    issue_count = fields.Integer()
    
    
