from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ["DATABASE_URL"]
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

from app.routes.machine_routes import machine_bp
from app.routes.issue_routes import issue_bp

app.register_blueprint(machine_bp, url_prefix='/api/machines')
app.register_blueprint(issue_bp, url_prefix='/api/issues')
